package blue.harvest.at;

import blue.harvest.at.model.Account;
import blue.harvest.at.model.TransactionStatus;
import blue.harvest.at.repository.AccountCrudRepository;
import blue.harvest.at.repository.TransactionCrudRepository;
import blue.harvest.at.repository.UserCrudRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.time.Instant;

import static blue.harvest.at.TestUtils.createTestUser;
import static blue.harvest.at.TestUtils.newTransaction;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@TestPropertySource(properties = "spring.sql.init.mode=never")
public class SimpleDataJpaTest {

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private AccountCrudRepository accountCrudRepository;

    @Autowired
    private TransactionCrudRepository transactionCrudRepository;

    @Test
    @DisplayName("Injected repositories are not null")
    void injectedComponentsAreNotNull(){
        assertThat(userCrudRepository).isNotNull();
        assertThat(accountCrudRepository).isNotNull();
        assertThat(transactionCrudRepository).isNotNull();
    }

    @Test
    @DisplayName("User can be created and stored")
    public void testUser() {
        assertThat(userCrudRepository.findAll()).isEmpty();
        assertThat(createTestUser(userCrudRepository)).isNotNull();

        var all = userCrudRepository.findAll();
        assertThat(all).hasSize(1);

        var user = all.iterator().next();
        assertThat(user).isNotNull();
        assertThat(user.getId()).isNotNull();
        assertThat(user.getUsername()).isEqualTo(TestUtils.TEST_USERNAME);
        assertThat(user.getPassword()).isEqualTo(TestUtils.TEST_PASSWORD);
        assertThat(user.getRole()).isEqualTo(TestUtils.TEST_USER_ROLE);
        assertThat(user.getAccountSet()).isEmpty();
        assertThat(user.getName()).isNull();
        assertThat(user.getSurname()).isNull();
    }

    @Test
    @DisplayName("Account can be created and stored")
    public void testAccount() {
        assertThat(userCrudRepository.findAll()).isEmpty();
        assertThat(accountCrudRepository.findAll()).isEmpty();

        var account = new Account();
        account.setCredit(new BigDecimal(0));
        account.setUser(createTestUser(userCrudRepository));
        account = accountCrudRepository.save(account);

        assertThat(account).isNotNull();
        assertThat(account.getId()).isNotNull();
    }

    @Test
    @DisplayName("Transaction can be created and stored")
    public void testTransaction() {
        assertThat(userCrudRepository.findAll()).isEmpty();
        assertThat(accountCrudRepository.findAll()).isEmpty();
        assertThat(transactionCrudRepository.findAll()).isEmpty();

        var account = new Account();
        account.setCredit(new BigDecimal(0));
        account.setUser(createTestUser(userCrudRepository));

        var amount = new BigDecimal("100.00");

        var transaction = newTransaction(accountCrudRepository.save(account), amount);
        transaction = transactionCrudRepository.save(transaction);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getStatus()).isEqualTo(TransactionStatus.SUCCESS);
        assertThat(transaction.getCreatedAt()).isNotNull().isEqualToIgnoringSeconds(Instant.now());
    }
}

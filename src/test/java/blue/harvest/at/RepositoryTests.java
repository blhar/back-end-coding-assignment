package blue.harvest.at;

import blue.harvest.at.model.Account;
import blue.harvest.at.model.Transaction;
import blue.harvest.at.model.UserRole;
import blue.harvest.at.repository.AccountCrudRepository;
import blue.harvest.at.repository.TransactionCrudRepository;
import blue.harvest.at.repository.UserCrudRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static blue.harvest.at.TestUtils.createTestUser;
import static blue.harvest.at.TestUtils.newTransaction;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@TestPropertySource(properties = "spring.sql.init.mode=never")
public class RepositoryTests {

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private AccountCrudRepository accountCrudRepository;

    @Autowired
    private TransactionCrudRepository transactionCrudRepository;

    private UUID adminUserId;
    private UUID customerUserId;

    private static final BigDecimal MAX_BALANCE_ADMIN = new BigDecimal(200);
    private static final BigDecimal MAX_BALANCE_CUSTOMER = new BigDecimal(10);

    @BeforeEach
    void before() {
        accountCrudRepository.deleteAll();
        userCrudRepository.deleteAll();

        // prefill admin user with three accounts
        var admin = createTestUser(userCrudRepository);
        this.adminUserId = admin.getId();

        var acc1 = new Account();
        acc1.setUser(admin);
        acc1.setCredit(new BigDecimal(0));
        var acc2 = new Account();
        acc2.setUser(admin);
        acc2.setCredit(MAX_BALANCE_ADMIN);
        var acc3 = new Account();
        acc3.setUser(admin);
        acc3.setCredit(new BigDecimal(100));
        accountCrudRepository.saveAll(List.of(acc1, acc2, acc3));

        // prefill customer with a single account
        var customer = createTestUser(userCrudRepository, "user", "password", UserRole.CUSTOMER);
        this.customerUserId = customer.getId();

        var acc4 = new Account();
        acc4.setCredit(MAX_BALANCE_CUSTOMER);
        acc4.setUser(customer);
        accountCrudRepository.save(acc4);
    }

    @Test
    @DisplayName("Get all accounts for single user")
    public void testGetAllAccountsForUser() {
        var all = accountCrudRepository.findAll();
        assertThat(all).hasSize(4);
        assertThat(all).containsAll(accountCrudRepository.findAllByUserId(this.adminUserId));
        assertThat(all).containsAll(accountCrudRepository.findAllByUserId(this.customerUserId));
    }

    @Test
    @DisplayName("Check maximal balance look up")
    public void testAccountReceiving() {
        var optAcc1 = accountCrudRepository.findTopAccountByCreditGreaterThanAndUserIdOrderByCreditDesc(
                BigDecimal.ZERO, adminUserId);
        assertThat(optAcc1).isPresent();
        assertThat(optAcc1.get().getCredit()).isEqualTo(MAX_BALANCE_ADMIN);

        var optAcc2 = accountCrudRepository.findTopAccountByCreditGreaterThanAndUserIdOrderByCreditDesc(
                BigDecimal.ZERO, customerUserId);
        assertThat(optAcc2).isPresent();
        assertThat(optAcc2.get().getCredit()).isEqualTo(MAX_BALANCE_CUSTOMER);
    }

    @Test
    @DisplayName("Check if getAllByUserId returns correct values")
    public void testTransactions() {
        // Fill admin account with max balance with some transactions
        var optAccAdminMax = accountCrudRepository.findTopAccountByCreditGreaterThanAndUserIdOrderByCreditDesc(
                BigDecimal.ZERO, this.adminUserId);
        assertThat(optAccAdminMax).isPresent();
        assertThat(optAccAdminMax.get().getCredit()).isEqualTo(MAX_BALANCE_ADMIN);

        // 5 + 10 + 3 + 24 = 42
        assertThat(transactionCrudRepository.saveAll(List.of(
                newTransaction(optAccAdminMax.get(), new BigDecimal(5)),
                newTransaction(optAccAdminMax.get(), new BigDecimal(10)),
                newTransaction(optAccAdminMax.get(), new BigDecimal(3)),
                newTransaction(optAccAdminMax.get(), new BigDecimal(24))
        ))).hasSize(4);

        // fill some another admin account with some transactions
        var optAnyOtherAccAdmin = accountCrudRepository.findAllByUserId(this.adminUserId)
                .stream()
                .filter(account -> account.getId().equals(optAccAdminMax.get().getId()))
                .findAny();
        assertThat(optAnyOtherAccAdmin).isPresent();

        // 1000 + 9 + 808 = 1817
        assertThat(transactionCrudRepository.saveAll(List.of(
                newTransaction(optAccAdminMax.get(), new BigDecimal(1000)),
                newTransaction(optAccAdminMax.get(), new BigDecimal(9)),
                newTransaction(optAccAdminMax.get(), new BigDecimal(808))
        ))).hasSize(3);

        // Fill customer account transactions
        var optAccCustomer = accountCrudRepository.findTopAccountByCreditGreaterThanAndUserIdOrderByCreditDesc(
                BigDecimal.ZERO, this.customerUserId);
        assertThat(optAccCustomer).isPresent();
        assertThat(optAccCustomer.get().getCredit()).isEqualTo(MAX_BALANCE_CUSTOMER);

        // 1 + 2 + 200 + 900 + 1000 = 2103
        assertThat(transactionCrudRepository.saveAll(List.of(
                newTransaction(optAccCustomer.get(), new BigDecimal(1)),
                newTransaction(optAccCustomer.get(), new BigDecimal(2)),
                newTransaction(optAccCustomer.get(), new BigDecimal(200)),
                newTransaction(optAccCustomer.get(), new BigDecimal(900)),
                newTransaction(optAccCustomer.get(), new BigDecimal(1000), true)
        ))).hasSize(5);

        // check if getAllByUserId for admin returns correct data
        var adminUserTransactions = transactionCrudRepository.findAllByUserId(this.adminUserId);
        assertThat(adminUserTransactions).hasSize(7);
        assertThat(adminUserTransactions).allMatch(transaction ->
                transaction.getUser().getId().equals(this.adminUserId));
        assertThat(adminUserTransactions.stream()
                .map(Transaction::getAmount)
                .reduce(BigDecimal::add)
        ).isPresent().hasValue(new BigDecimal(5 + 10 + 3 + 24 + 1000 + 9 + 808));

        // check if getAllByUserId for customer returns correct data
        var customerUserTransactions = transactionCrudRepository.findAllByUserId(this.customerUserId);
        assertThat(customerUserTransactions).hasSize(5);
        assertThat(customerUserTransactions).allMatch(transaction ->
                transaction.getUser().getId().equals(this.customerUserId));
        assertThat(customerUserTransactions.stream()
                .map(Transaction::getAmount)
                .reduce(BigDecimal::add)
        ).isPresent().hasValue(new BigDecimal( 1 + 2 + 200 + 900 + 1000));
    }
}

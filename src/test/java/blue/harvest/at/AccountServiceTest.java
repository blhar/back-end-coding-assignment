package blue.harvest.at;

import blue.harvest.at.model.User;
import blue.harvest.at.repository.AccountCrudRepository;
import blue.harvest.at.repository.UserCrudRepository;
import blue.harvest.at.service.AccountService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
@Transactional
@Execution(ExecutionMode.CONCURRENT)
public class AccountServiceTest {

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private AccountCrudRepository accountCrudRepository;

    @Autowired
    private AccountService accountService;

    @Test
    @DisplayName("Happy path add new Account to pre-created used")
    public void testAddNewAccountToUser() {
        var users = userCrudRepository.findAll();
        assertThat(users).isNotNull().hasSize(2);
        var user = users.iterator().next();
        assertThat(user.getId()).isNotNull();
        assertThat(user.getBalance()).isZero();

        var firstCredit = new BigDecimal("120.25");
        var secondCredit = new BigDecimal("149.75");

        var account = accountService.addAccountToUser(user.getId(), firstCredit);
        assertThat(account).isNotNull();
        assertThat(userCrudRepository.findById(user.getId()).orElseThrow().getBalance())
                .isEqualTo(firstCredit);

        accountService.addAccountToUser(user.getId(), secondCredit);
        assertThat(userCrudRepository.findById(user.getId()).orElseThrow().getBalance())
                .isEqualTo(firstCredit.add(secondCredit));
    }

    @Test
    @DisplayName("Not existing user account adding attempt")
    public void testExceptionFired() {
        var uuid = UUID.randomUUID();
        assertThatThrownBy(() -> accountService.addAccountToUser(uuid, BigDecimal.ONE))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(uuid.toString());
        assertTrue(TestTransaction.isFlaggedForRollback());
    }

    @Test
    @DisplayName("Transaction failed - consistency check")
    public void testConsistentDBWhenTransactionFailed() {
        assertTrue(TestTransaction.isActive());
        var user = userCrudRepository.findAll().iterator().next();
        var uuid = user.getId();

        // prepare fault injection
        var mockedUserCrudRepository = Mockito.mock(UserCrudRepository.class);
        when(mockedUserCrudRepository.findByIdPessimistic(eq(uuid))).thenReturn(Optional.of(user));
        when(mockedUserCrudRepository.save(any(User.class)))
                .thenThrow(new IllegalStateException("#FAILED"));

        var service = new AccountService(accountCrudRepository, mockedUserCrudRepository);

        assertThatThrownBy(() -> service.addAccountToUser(uuid, BigDecimal.valueOf(993)))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("#FAILED");
        // make sure transaction is going to roll back and end it
        assertTrue(TestTransaction.isFlaggedForRollback());
        TestTransaction.end();
        assertFalse(TestTransaction.isActive());

        // check the consistency of the data
        user = userCrudRepository.findAll().iterator().next();
        assertThat(user.getBalance()).isZero();
        assertThat(accountCrudRepository.findAll()).isNotNull().isEmpty();
    }
}

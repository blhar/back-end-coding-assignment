package blue.harvest.at;

import blue.harvest.at.model.Account;
import blue.harvest.at.model.Transaction;
import blue.harvest.at.model.TransactionStatus;
import blue.harvest.at.model.User;
import blue.harvest.at.model.UserRole;
import blue.harvest.at.repository.UserCrudRepository;

import java.math.BigDecimal;

public class TestUtils {

    static final String TEST_USERNAME = "test_user";
    static final String TEST_PASSWORD = "test_password";
    static final UserRole TEST_USER_ROLE = UserRole.ADMIN;

    private TestUtils () {}

    static User createTestUser(UserCrudRepository userCrudRepository,
                               String username, String password, UserRole role) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        return userCrudRepository.save(user);
    }

    static User createTestUser(UserCrudRepository userCrudRepository) {
        return createTestUser(userCrudRepository, TEST_USERNAME, TEST_PASSWORD, TEST_USER_ROLE);
    }

    static Transaction newTransaction(Account account, BigDecimal amount) {
        return newTransaction(account, amount, false);
    }

    static Transaction newTransaction(Account account, BigDecimal amount, boolean noAccount) {
        var tr = new Transaction();
        tr.setUser(account.getUser());
        tr.setAmount(amount);
        tr.setStatus(TransactionStatus.SUCCESS);
        if (!noAccount) {
            tr.setAccount(account);
        }
        return tr;
    }

}

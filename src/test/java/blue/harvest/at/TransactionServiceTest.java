package blue.harvest.at;

import blue.harvest.at.model.Transaction;
import blue.harvest.at.model.TransactionStatus;
import blue.harvest.at.model.User;
import blue.harvest.at.repository.AccountCrudRepository;
import blue.harvest.at.repository.TransactionCrudRepository;
import blue.harvest.at.repository.UserCrudRepository;
import blue.harvest.at.service.AccountService;
import blue.harvest.at.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
@Transactional
public class TransactionServiceTest {

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private AccountCrudRepository accountCrudRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionCrudRepository transactionCrudRepository;

    private User user;

    @BeforeEach
    public void prepare() {
        var allUsers = userCrudRepository.findAll();
        assertThat(allUsers).isNotNull().hasSize(2);
        this.user = allUsers.iterator().next();
        // prerequisites check - everything should be empty
        assertThat(transactionCrudRepository.findAllByUserId(user.getId())).isNotNull().isEmpty();
        assertThat(accountCrudRepository.findAllByUserId(user.getId())).isNotNull().isEmpty();
    }

    @Test
    @DisplayName("Happy path add three accounts and perform a transaction, some money left")
    public void testHappyPath() {
        // add accounts with some credits
        var amount = new BigDecimal("450.00");
        var fAccountAmount = new BigDecimal("300.00");
        var sAccountAmount = new BigDecimal("100.00");
        var tAccountAmount = new BigDecimal("70.00");
        var accSumAmount = fAccountAmount.add(sAccountAmount).add(tAccountAmount);
        assertThat(accountService.addAccountToUser(user.getId(), fAccountAmount)).isNotNull();
        assertThat(accountService.addAccountToUser(user.getId(), sAccountAmount)).isNotNull();
        assertThat(accountService.addAccountToUser(user.getId(), tAccountAmount)).isNotNull();

        // check user balance
        var optUser = userCrudRepository.findById(user.getId());
        assertThat(optUser).isPresent();
        assertThat(optUser.get().getBalance()).isEqualTo(accSumAmount);

        // perform a transaction
        assertThat(transactionService.makeTransaction(user.getId(), amount)).isNotNull();

        // check all entities we have now
        assertThat(userCrudRepository.findById(user.getId()).orElseThrow().getBalance())
                .isEqualTo(accSumAmount.subtract(amount));
        var transactions = transactionCrudRepository.findAllByUserId(user.getId());
        assertThat(transactions).isNotNull().hasSize(4);
        assertThat(transactions).allMatch(transaction -> transaction.getCreatedAt() != null);

        var mainTrList = transactions.stream().filter(t -> !t.getChildren().isEmpty()).collect(Collectors.toList());
        assertThat(mainTrList).isNotNull().hasSize(1);

        var subTrList = transactions.stream().filter(t -> t.getParent() != null).collect(Collectors.toList());
        assertThat(subTrList).isNotNull().hasSize(3);

        assertThat(mainTrList.get(0).getAmount()).isEqualTo(amount);
        assertThat(mainTrList.get(0).getStatus()).isEqualTo(TransactionStatus.SUCCESS);
        assertThat(subTrList.stream().map(Transaction::getAmount).reduce(BigDecimal::add).orElseThrow())
                .isEqualTo(amount);
        assertThat(subTrList).allMatch(t -> t.getParent().getId().equals(mainTrList.get(0).getId()));
        assertThat(subTrList).allMatch(t -> t.getStatus().equals(TransactionStatus.SUCCESS));
    }

    @Test
    @DisplayName("Happy path add three accounts and perform a transaction, no money left")
    public void testExactAmountSpent() {
        var fAccountAmount = new BigDecimal("300.00");
        var sAccountAmount = new BigDecimal("100.00");
        var tAccountAmount = new BigDecimal("70.00");
        var amount = fAccountAmount.add(sAccountAmount).add(tAccountAmount);

        assertThat(accountService.addAccountToUser(user.getId(), fAccountAmount)).isNotNull();
        assertThat(accountService.addAccountToUser(user.getId(), sAccountAmount)).isNotNull();
        assertThat(accountService.addAccountToUser(user.getId(), tAccountAmount)).isNotNull();
        assertThat(transactionService.makeTransaction(user.getId(), amount)).isNotNull();

        // check all entities we have now
        assertThat(userCrudRepository.findById(user.getId()).orElseThrow().getBalance()).isZero();
    }

    private void assertFailedTransaction(Transaction tr, BigDecimal amount,  String ... args) {
        assertThat(tr).isNotNull();
        assertThat(tr.getStatus()).isEqualTo(TransactionStatus.FAILED);
        assertThat(tr.getParent()).isNull();
        assertThat(tr.getChildren()).isEmpty();
        assertThat(tr.getUser().getId()).isEqualTo(user.getId());
        assertThat(tr.getId()).isNotNull();
        assertThat(tr.getAccount()).isNull();
        assertThat(tr.getAmount()).isEqualTo(amount);
        assertThat(tr.getCreatedAt()).isEqualToIgnoringSeconds(Instant.now());
    }

    @Test
    @DisplayName("No account failed transaction")
    public void testNoAccount() {
        var amount = new BigDecimal("450.00");
        assertFailedTransaction(transactionService.makeTransaction(user.getId(), amount),
                amount, "no account", user.getId().toString());
    }

    @Test
    @DisplayName("Invalid amount failed transaction")
    public void testInvalidAmount() {
        var amount = new BigDecimal("0.00");
        assertFailedTransaction(transactionService.makeTransaction(user.getId(), amount),
                amount, "Invalid amount", amount.toPlainString());
    }

    @Test
    @DisplayName("Not enough balance to pay for amount failed transaction")
    public void testInsufficientFunds() {
        var fAccountAmount = new BigDecimal("300.00");
        var sAccountAmount = new BigDecimal("100.00");
        var tAccountAmount = new BigDecimal("70.00");
        assertThat(accountService.addAccountToUser(user.getId(), fAccountAmount)).isNotNull();
        assertThat(accountService.addAccountToUser(user.getId(), sAccountAmount)).isNotNull();
        assertThat(accountService.addAccountToUser(user.getId(), tAccountAmount)).isNotNull();

        var amount = fAccountAmount.add(sAccountAmount).add(tAccountAmount).add(BigDecimal.ONE);
        assertFailedTransaction(transactionService.makeTransaction(user.getId(), amount),
                amount, "not enough funds", user.getId().toString(), amount.toPlainString());
    }

    @Test
    @DisplayName("Happy path GetTransactionListByUserId")
    public void testHappyPathGetTransactionListByUserId() {
        assertThat(transactionService.getTransactionListByUserId(user.getId())).isNotNull().isEmpty();
        var tr = new Transaction();
        tr.setUser(user);
        tr.setAmount(BigDecimal.ONE);
        tr.setStatus(TransactionStatus.SUCCESS);
        tr = transactionCrudRepository.save(tr);
        assertThat(tr).isNotNull();
        assertThat(transactionService.getTransactionListByUserId(user.getId())).isNotNull().hasSize(1);
    }

    @Test
    @DisplayName("Invalid UserId GetTransactionListByUserId")
    public void testInvalidUserIdGetTransactionListByUserId() {
        var uuid = UUID.randomUUID();
        assertThatThrownBy(() -> transactionService.getTransactionListByUserId(uuid))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining(uuid.toString());
    }

}

package blue.harvest.at;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApplicationTests {

    @Test
    @DisplayName("Context itself is valid")
    void contextLoads() {
    }
}

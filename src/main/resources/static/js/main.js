$(document).ready(function() {
    $(".art").hide();
    const userArticle = $(".user-area");
    const accountArticle = $(".account-area");
    const transactionArticle = $(".transaction-area");
    var principal;
    var usrTable

    function isAdmin() {
        return principal.role === "ADMIN";
    }

    function drawUserInfo(user) {
        const mainInfoUser = $("#mainInfoUser").empty();
        console.log("Not admin...");
        var element = $("<li>").addClass("grid");
        element.append("<div><b>User ID:</b></div>");
        element.append("<div>" + user.id + "</div>")
        mainInfoUser.append(element);

        element = $("<li>").addClass("grid");
        element.append("<div><b>Username:</b></div>");
        element.append("<div>" + user.username + "</div>")
        mainInfoUser.append(element);

        element = $("<li>").addClass("grid");
        element.append("<div><b>First Name:</b></div>");
        element.append("<div>" + user.firstName + "</div>")
        mainInfoUser.append(element);

        element = $("<li>").addClass("grid");
        element.append("<div><b>Last Name:</b></div>");
        element.append("<div>" + user.lastName + "</div>")
        mainInfoUser.append(element);

        element = $("<li>").addClass("grid");
        element.append("<div><b>Balance:</b></div>");
        element.append("<div>" + user.balance + "</div>")
        mainInfoUser.append(element);

        element = $("<li>").addClass("grid");
        element.append("<div><b>Role:</b></div>");
        element.append("<div>" + user.role + "</div>")
        mainInfoUser.append(element);
    }

    function loadCurrentUser() {
        $.ajax({
            type: "GET",
            dataType: "json",
            async: false,
            url: "/api/rest/user/current",
            success: function(user) {
                principal = user;
                userArticle.show();
                if (!isAdmin()) {
                    $("#userTableRow").hide();
                    drawUserInfo(user);

                }
            },
            error: function(result) {
                console.error(result);
                alert('Error occurred!');
            }
        });
    }

    loadCurrentUser();

    if (isAdmin()) { // if admin - draw initially the table
        usrTable = $('#userTable').DataTable( {
            paging: false,
            searching: false,
            ajax: {
                url: '/api/rest/user',
                dataSrc: '',
                error: function (xhr, error, code) {
                    console.log(xhr, code);
                }
            },
            deferRender: true,
            columns: [
                { data: 'id' },
                { data: 'username' },
                { data: 'balance' },
                { data: 'role' },
                { data: 'firstName' },
                { data: 'lastName' }
            ]
        });
    } else { // otherwise - hide it
        $("#userTableRow").hide();
    }
    
    const accTable = $('#accountTable').DataTable( {
        paging: false,
        searching: false,
        ajax: {
            url: '/api/rest/user/current/account',
            dataSrc: ''
        },
        deferRender: true,
        columns: [
            { data: 'id' },
            { data: 'credit' }
        ]
    });

    const trTable = $('#transactionTable').DataTable( {
        paging: false,
        searching: false,
        ajax: {
            url: '/api/rest/user/current/transaction',
            dataSrc: ''
        },
        deferRender: true,
        columns: [
            { data: 'id' },
            { data: 'parentId' },
            { data: 'accountId' },
            { data: 'amount' },
            { data: 'created_at' },
            { data: 'status' },
            { data: 'details' }
        ]
    });

    $("a.user").click(function(e) {
        e.preventDefault();
        $(".art").hide();
        userArticle.show();
        if (isAdmin()) {
            usrTable.ajax.reload();
        } else {
            loadCurrentUser();
        }
    });

    $("a.account").click(function(e) {
        e.preventDefault();
        $(".art").hide();
        accountArticle.show();
        accTable.ajax.reload();
    });

    $("a.transaction").click(function(e) {
        e.preventDefault();
        $(".art").hide();
        transactionArticle.show();
        trTable.ajax.reload();
    });

    $("#accForm").submit(function (event) {
        $.ajax({
            type: "POST",
            url: "/api/rest/user/current/account",
            data: JSON.stringify({
                initialCredit: $("#initialCredit").val()
            }),
            dataType: "json",
            contentType: "application/json;charset=utf-8",
        }).done(function (data) {
            console.log(data);
            accTable.ajax.reload();
        });

        event.preventDefault();
    });

    $("#trForm").submit(function (event) {
        $.ajax({
            type: "POST",
            url: "/api/rest/user/current/transaction",
            data: JSON.stringify({
                amount: $("#amount").val()
            }),
            dataType: "json",
            contentType: "application/json;charset=utf-8",
        }).done(function (data) {
            console.log(data);
            trTable.ajax.reload();
        });

        event.preventDefault();
    });
});


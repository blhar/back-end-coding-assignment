package blue.harvest.at.service;

import blue.harvest.at.model.Transaction;
import blue.harvest.at.model.TransactionStatus;
import blue.harvest.at.model.User;
import blue.harvest.at.repository.AccountCrudRepository;
import blue.harvest.at.repository.TransactionCrudRepository;
import blue.harvest.at.repository.UserCrudRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class TransactionService {

    private final AccountCrudRepository accountCrudRepository;
    private final UserCrudRepository userCrudRepository;
    private final TransactionCrudRepository transactionCrudRepository;

    @Transactional
    public Transaction makeTransaction(UUID userId, BigDecimal amount) {
        log.debug("Attempt to charge user {} for {}", userId, amount.toPlainString());
        Transaction transaction = new Transaction();
        var user = userCrudRepository.findByIdPessimistic(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Entity %s with ID %s does not exist",
                        User.class.getSimpleName(), userId)));

        transaction.setUser(user);
        transaction.setAmount(amount);

        var accounts = accountCrudRepository.findAllByUserAndCreditGreaterThanOrderByCreditDesc(user, BigDecimal.ZERO);

        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            // amount must be greater than zero
            transaction.setStatus(TransactionStatus.FAILED);
            transaction.setDetails(String.format("Invalid amount passed: %s", amount.toPlainString()));
        } else  if (accounts == null || accounts.isEmpty()) {
            // we don't support users w/o any accounts at all
            transaction.setStatus(TransactionStatus.FAILED);
            transaction.setDetails(String.format("User %s has no account. Must be at least one.", userId));
        } else if (user.getBalance().compareTo(amount) < 0) {
            // already know balance is insufficient - don't even make an attempt
            transaction.setStatus(TransactionStatus.FAILED) ;
            transaction.setDetails(String.format("User %s has not enough funds to perform transaction of amount %s",
                    userId, amount.toPlainString()));
        } else {
            var cAmount = amount;
            var iterAcc = accounts.iterator();
            while (cAmount.compareTo(BigDecimal.ZERO) != 0 && iterAcc.hasNext()) {
                var cAcc = iterAcc.next();
                var cTransaction = new Transaction();
                cTransaction.setUser(user);
                cTransaction.setStatus(TransactionStatus.SUCCESS);
                cTransaction.setAccount(cAcc);
                cTransaction.setParent(transaction);

                BigDecimal creditsSpent;
                if (cAcc.getCredit().compareTo(cAmount) < 0) { // we cannot go negative
                    creditsSpent = cAcc.getCredit();
                    cAcc.setCredit(BigDecimal.ZERO);
                } else {
                    // we have enough credits to pay for the rest of amount
                    creditsSpent = cAmount;
                    cAcc.setCredit(cAcc.getCredit().subtract(cAmount));
                }
                cTransaction.setAmount(creditsSpent);
                accountCrudRepository.save(cAcc);
                transaction.getChildren().add(cTransaction);
                cAmount = cAmount.subtract(creditsSpent);
                log.debug("{} funds were written off from {} account, account now has balance of {}",
                        creditsSpent.toPlainString(), cAcc.getId(), cAcc.getCredit().toPlainString());
            }
            // if there are some funds not covered by accounts transaction - throw.
            if (cAmount.compareTo(BigDecimal.ZERO) != 0) {
                throw new IllegalStateException(
                        String.format("Could not perform transaction for %s funds as accounts " +
                                "inconsistency occurred for user %s", amount.toPlainString(), userId));
            }
            // update user's balance
            user.setBalance(user.getBalance().subtract(amount));
            userCrudRepository.save(user);
            transaction.setStatus(TransactionStatus.SUCCESS);
        }
        log.info("User {} performed main transaction of amount {} with status {}", userId, amount.toPlainString(),
                transaction.getStatus());
        return transactionCrudRepository.save(transaction);
    }

    @Transactional(readOnly = true)
    public Iterable<Transaction> getTransactionListByUserId(UUID userId) {
        userCrudRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Entity %s with ID %s does not exist",
                        User.class.getSimpleName(), userId)));

        return transactionCrudRepository.findAllByUserId(userId);
    }

    @Transactional(readOnly = true)
    public Iterable<Transaction> getAllTransactions(Pageable pageable) {
        return transactionCrudRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Transaction getTransactionById(UUID id) {
        return transactionCrudRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Entity %s with ID %s does not exist",
                        Transaction.class.getSimpleName(), id)));
    }
}

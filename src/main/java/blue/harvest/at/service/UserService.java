package blue.harvest.at.service;

import blue.harvest.at.model.User;
import blue.harvest.at.repository.UserCrudRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class UserService {

    private final UserCrudRepository userCrudRepository;

    public User getUserById(UUID userId) {
        return userCrudRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Entity %s with ID %s does not exist",
                        User.class.getSimpleName(), userId)));
    }

    public User getUserByUsername(String username) {
        return userCrudRepository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format("%s with name %s does not exist",
                        User.class.getSimpleName(), username)));
    }

    public Iterable<User> getUserList(Pageable pageable) {
        return userCrudRepository.findAll(pageable);
    }
}

package blue.harvest.at.service;

import blue.harvest.at.model.Account;
import blue.harvest.at.model.User;
import blue.harvest.at.repository.AccountCrudRepository;
import blue.harvest.at.repository.UserCrudRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class AccountService {

    private final AccountCrudRepository accountCrudRepository;
    private final UserCrudRepository userCrudRepository;

    @Transactional
    public Account addAccountToUser(UUID userId, BigDecimal initialCredit) {
        log.debug("About to add new account with initialCredit {} to user {}",
                initialCredit.toPlainString(), userId);
        var user = userCrudRepository.findByIdPessimistic(userId)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Entity %s with ID %s does not exist",
                                User.class.getSimpleName(), userId)));

        var account = new Account();
        account.setUser(user);
        account.setCredit(initialCredit);
        account = accountCrudRepository.save(account);
        user.setBalance(user.getBalance().add(initialCredit));
        userCrudRepository.save(user);
        log.info("Account {} with credit {} has been created for user {}. Balance: {}",
                account.getId(), account.getCredit(), userId, user.getBalance().toPlainString());
        return account;
    }

    @Transactional(readOnly = true)
    public Iterable<Account> getAllAccounts(Pageable pageable) {
        return accountCrudRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Account getAccountById(UUID id) {
        return accountCrudRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Entity %s with ID %s does not exist",
                        Account.class.getSimpleName(), id)));
    }
}

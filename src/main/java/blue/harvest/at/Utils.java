package blue.harvest.at;

import blue.harvest.at.controller.ApiError;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Utils {
    private Utils() {}

    public static final String BASE_REST_PATH = "/api/rest/";
    public static final String USER_PATH = "user";
    public static final String ACCOUNT_PATH = "account";
    public static final String TRANSACTION_PATH = "transaction";

    public static <T,R> Iterable<R> mapIterable(Iterable<T> input, Function<T,R> mapper) {
        return StreamSupport.stream(input.spliterator(), false)
                .map(mapper)
                .collect(Collectors.toList());
    }

    public static Pageable fromParams(int page, int size, String [] sort) {
        var s = sort[1].equalsIgnoreCase("asc") ?
                Sort.by(sort[0]).ascending() :
                Sort.by(sort[0]).descending();
        return PageRequest.of(page, size, s);
    }

    public static ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, apiError.getStatus());
    }

}

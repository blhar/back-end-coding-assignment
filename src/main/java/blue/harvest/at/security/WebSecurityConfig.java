package blue.harvest.at.security;

import blue.harvest.at.model.UserRole;
import blue.harvest.at.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig {
    private final UserService userService;
    private static final String [] ALLOWED_AUTHENTICATED_GET = {"/*", "/css/*", "/js/*", "/api/rest/user/current/**"};
    private static final String [] ALLOWED_AUTHENTICATED_POST = {"/api/rest/user/current/transaction",
            "/api/rest/user/current/account", "/api/rest/transaction", "/api/rest/account"};

    @Bean
    public UserDetailsService getUserDetailsService() {
        return new UserDetailsServiceImpl(userService);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
         http.csrf().disable()
                 .authorizeHttpRequests()
                    .antMatchers(HttpMethod.GET, ALLOWED_AUTHENTICATED_GET)
                    .authenticated()
                 .and()
                 .authorizeHttpRequests()
                 .antMatchers(HttpMethod.POST, ALLOWED_AUTHENTICATED_POST)
                 .authenticated()
                 .anyRequest()
                    .hasAuthority(UserRole.ADMIN.name())
                 .and().httpBasic();
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}

package blue.harvest.at.dto;

import blue.harvest.at.model.Account;
import blue.harvest.at.model.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.Date;
import java.util.UUID;
import java.util.function.Function;

import static java.util.Optional.ofNullable;

@Data
@JsonPropertyOrder({ "id", "parentId", "accountId", "amount", "created_at", "status", "details"})
public class TransactionDTO {
    private final String id;
    private final String parentId;
    private final String accountId;
    private final String amount;
    @JsonProperty("created_at") private final Date createdAt;
    private final String status;
    private final String details;


    public static Function<Transaction, TransactionDTO> fromTransaction = tr ->
            new TransactionDTO(
                    tr.getId().toString(),
                    ofNullable(tr.getParent())
                            .map(Transaction::getId)
                            .map(UUID::toString)
                            .orElse(null),
                    ofNullable(tr.getAccount())
                            .map(Account::getId)
                            .map(UUID::toString)
                            .orElse(null),
                    tr.getAmount().toPlainString(),
                    tr.getCreatedAt(),
                    tr.getStatus().name(),
                    tr.getDetails()
            );
}

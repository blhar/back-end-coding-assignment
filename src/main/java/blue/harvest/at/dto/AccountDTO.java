package blue.harvest.at.dto;

import blue.harvest.at.model.Account;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.function.Function;

@Data
@JsonPropertyOrder({ "id", "credit"})
public class AccountDTO {
    private final String id;
    private final String credit;

    public static Function<Account, AccountDTO> fromAccount = acc ->
            new AccountDTO(acc.getId().toString(), acc.getCredit().toPlainString());
}

package blue.harvest.at.dto;

import blue.harvest.at.model.User;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.function.Function;

@Data
@JsonPropertyOrder({ "id", "username", "firstName", "lastName", "balance", "role"})
public class UserDTO {
    private final String id;
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String balance;
    private final String role;

    public static Function<User, UserDTO> fromUser = user ->
            new UserDTO(
                user.getId().toString(),
                user.getUsername(),
                user.getName(),
                user.getSurname(),
                user.getBalance().toPlainString(),
                user.getRole().name()
             );
}

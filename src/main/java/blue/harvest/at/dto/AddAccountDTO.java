package blue.harvest.at.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter @Setter @NoArgsConstructor
public class AddAccountDTO {
    @NotEmpty
    private String initialCredit;
}

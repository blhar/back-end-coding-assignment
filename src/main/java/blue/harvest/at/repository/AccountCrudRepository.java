package blue.harvest.at.repository;

import blue.harvest.at.model.Account;
import blue.harvest.at.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AccountCrudRepository extends PagingAndSortingRepository<Account, UUID> {
    List<Account> findAllByUserId(UUID id);
    List<Account> findAllByUserAndCreditGreaterThanOrderByCreditDesc(User user, BigDecimal credit);
    Optional<Account> findTopAccountByCreditGreaterThanAndUserIdOrderByCreditDesc(BigDecimal credit, UUID id);
}

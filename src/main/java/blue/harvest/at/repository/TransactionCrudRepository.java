package blue.harvest.at.repository;

import blue.harvest.at.model.Transaction;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionCrudRepository extends PagingAndSortingRepository<Transaction, UUID> {
    List<Transaction> findAllByUserId(UUID id);
}

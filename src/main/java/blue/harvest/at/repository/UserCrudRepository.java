package blue.harvest.at.repository;

import blue.harvest.at.model.User;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserCrudRepository extends PagingAndSortingRepository<User, UUID> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT u from User as u WHERE u.id = ?1")
    Optional<User> findByIdPessimistic(UUID id);

    Optional<User> findByUsername(String username);
}

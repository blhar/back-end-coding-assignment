package blue.harvest.at.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import static blue.harvest.at.Utils.buildResponseEntity;

@Controller
public class CommonErrorController implements ErrorController {

    @RequestMapping("/error")
    public ResponseEntity<Object> error(HttpServletRequest request) {
        var status = getStatus(request);
        return buildResponseEntity(new ApiError(status));
    }

    private static HttpStatus getStatus(HttpServletRequest request) {
        var statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        try {
            return HttpStatus.valueOf(statusCode);
        } catch (Exception ex) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}

package blue.harvest.at.controller;

import blue.harvest.at.Utils;
import blue.harvest.at.dto.AccountDTO;
import blue.harvest.at.dto.AddAccountDTO;
import blue.harvest.at.service.AccountService;
import blue.harvest.at.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.UUID;

import static blue.harvest.at.Utils.ACCOUNT_PATH;
import static blue.harvest.at.Utils.BASE_REST_PATH;
import static blue.harvest.at.Utils.USER_PATH;
import static blue.harvest.at.Utils.fromParams;

@Controller
@RequestMapping(
        value = BASE_REST_PATH,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@AllArgsConstructor
@ResponseBody
public class AccountController {
    private final UserService userService;
    private final AccountService accountService;

    @GetMapping(USER_PATH + "/current/" + ACCOUNT_PATH)
    Iterable<AccountDTO> getCurrentUserAccount() {
        var current = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Utils.mapIterable(userService.getUserByUsername(current.getUsername()).getAccountSet(),
                AccountDTO.fromAccount);
    }

    @GetMapping(ACCOUNT_PATH)
    Iterable<AccountDTO> getAllAccounts(@RequestParam(defaultValue = "0") int page,
                                        @RequestParam(defaultValue = "20") int size,
                                        @RequestParam(defaultValue = "credit,desc") String[] sort) {
        return Utils.mapIterable(accountService.getAllAccounts(fromParams(page, size, sort)),
                AccountDTO.fromAccount);
    }

    @GetMapping(ACCOUNT_PATH + "/{uuid}")
    AccountDTO getAccountById(@PathVariable("uuid") String uuid) {
        return AccountDTO.fromAccount.apply(accountService.getAccountById(UUID.fromString(uuid)));
    }

    @PostMapping(value = USER_PATH + "/{uuid}/" + ACCOUNT_PATH,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    AccountDTO addNewAccountToUserById(@PathVariable("uuid") String uuid,
                                       @Valid @RequestBody AddAccountDTO addAccountDTO) {
        UUID id;
        if (uuid.equalsIgnoreCase("current")) {
            var current = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            id = userService.getUserByUsername(current.getUsername()).getId();
        } else {
            id = UUID.fromString(uuid);
        }
        var initialCredit = new BigDecimal(addAccountDTO.getInitialCredit());
        return AccountDTO.fromAccount.apply(accountService.addAccountToUser(id, initialCredit));
    }

}

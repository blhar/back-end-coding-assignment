package blue.harvest.at.controller;

import blue.harvest.at.Utils;
import blue.harvest.at.dto.UserDTO;
import blue.harvest.at.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;

import static blue.harvest.at.Utils.BASE_REST_PATH;
import static blue.harvest.at.Utils.USER_PATH;
import static blue.harvest.at.Utils.fromParams;

@Controller
@RequestMapping(
        value = BASE_REST_PATH + USER_PATH,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@AllArgsConstructor
@ResponseBody
public class UserController {
    private final UserService userService;

    @GetMapping(path = {"/", ""})
    Iterable<UserDTO> getAllUsers(@RequestParam(defaultValue = "0") int page,
                                  @RequestParam(defaultValue = "20") int size,
                                  @RequestParam(defaultValue = "balance,desc") String[] sort) {
        return Utils.mapIterable(userService.getUserList(fromParams(page, size, sort)), UserDTO.fromUser);
    }

    @GetMapping("/current")
    UserDTO getCurrentUser() {
        var current = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return UserDTO.fromUser.apply(userService.getUserByUsername(current.getUsername()));
    }

    @GetMapping("/{uuid}")
    UserDTO getUserById(@PathVariable("uuid") String uuid) {
       return UserDTO.fromUser.apply(userService.getUserById(UUID.fromString(uuid)));
    }

}

package blue.harvest.at.controller;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter @Setter
public class ApiError {
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp = LocalDateTime.now();
    private String message;

    ApiError(HttpStatus status) {
        this(status, getMessageByStatus(status));
    }

    ApiError(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    private static String getMessageByStatus(HttpStatus status) {
        String message;
        switch (status) {
            case NOT_FOUND:
                message = "Requested resource not found";
                break;
            case UNAUTHORIZED:
                message = "Only authenticated users are allowed to use the application";
                break;
            case FORBIDDEN:
                message = "Current user has not enough permissions to use requested resource";
                break;
            default:
                message = "Unexpected error occurred";
        }
        return message;
    }
}
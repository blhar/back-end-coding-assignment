package blue.harvest.at.controller;

import blue.harvest.at.Utils;
import blue.harvest.at.dto.MakeTransactionDTO;
import blue.harvest.at.dto.TransactionDTO;
import blue.harvest.at.service.TransactionService;
import blue.harvest.at.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.UUID;

import static blue.harvest.at.Utils.BASE_REST_PATH;
import static blue.harvest.at.Utils.TRANSACTION_PATH;
import static blue.harvest.at.Utils.USER_PATH;
import static blue.harvest.at.Utils.fromParams;

@Controller
@RequestMapping(
        value = BASE_REST_PATH,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@AllArgsConstructor
@ResponseBody
public class TransactionController {
    private final UserService userService;
    private final TransactionService transactionService;

    @GetMapping(USER_PATH + "/current/" + TRANSACTION_PATH)
    Iterable<TransactionDTO> getCurrentUserTransactions() {
        var current = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Utils.mapIterable(userService.getUserByUsername(current.getUsername()).getTransactionSet(),
                TransactionDTO.fromTransaction);
    }

    @GetMapping(TRANSACTION_PATH)
    Iterable<TransactionDTO> getAllTransactions(@RequestParam(defaultValue = "0") int page,
                                        @RequestParam(defaultValue = "20") int size,
                                        @RequestParam(defaultValue = "amount,desc") String[] sort) {
        return Utils.mapIterable(transactionService.getAllTransactions(fromParams(page, size, sort)),
                TransactionDTO.fromTransaction);
    }

    @GetMapping(TRANSACTION_PATH + "/{uuid}")
    TransactionDTO getTransactionById(@PathVariable("uuid") String uuid) {
        return TransactionDTO.fromTransaction.apply(transactionService.getTransactionById(UUID.fromString(uuid)));
    }

    @PostMapping(value = USER_PATH + "/{uuid}/" + TRANSACTION_PATH,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    TransactionDTO addNewAccountToUserById(@PathVariable("uuid") String uuid,
                                           @Valid @RequestBody MakeTransactionDTO makeTransactionDTO) {
        UUID id;
        if (uuid.equalsIgnoreCase("current")) {
            var current = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            id = userService.getUserByUsername(current.getUsername()).getId();
        } else {
            id = UUID.fromString(uuid);
        }

        var amount = new BigDecimal(makeTransactionDTO.getAmount());
        return TransactionDTO.fromTransaction.apply(transactionService.makeTransaction(id, amount));
    }
}

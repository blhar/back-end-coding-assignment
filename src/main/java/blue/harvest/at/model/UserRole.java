package blue.harvest.at.model;

public enum UserRole {
    ADMIN,
    CUSTOMER
}

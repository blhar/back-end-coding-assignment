package blue.harvest.at.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="app_user")
@Getter @Setter @NoArgsConstructor @ToString(callSuper=true)
public class User extends UUIDEntity {

    @Column(name="username", length=50, nullable=false, unique=true)
    private String username;

    @Column(name="password", length=100, nullable=false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name="role", nullable=false)
    private UserRole role;

    @Column(name="name", length=100)
    private String name;

    @Column(name="surname", length=100)
    private String surname;

    @Column(name = "balance", nullable = false, insertable = false)
    @ColumnDefault("0")
    private BigDecimal balance = BigDecimal.ZERO;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private Set<Account> accountSet = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private Set<Transaction> transactionSet = new HashSet<>();

}

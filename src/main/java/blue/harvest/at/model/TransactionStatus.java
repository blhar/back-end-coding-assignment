package blue.harvest.at.model;

public enum TransactionStatus {
    SUCCESS,
    FAILED
}

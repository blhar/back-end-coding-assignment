package blue.harvest.at.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
@Getter @Setter @ToString
public class UUIDEntity {

    @Id
    @GeneratedValue(generator = "UUID", strategy = GenerationType.TABLE)
    @ColumnDefault("random_uuid()")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, insertable = false, columnDefinition = "BINARY(16)")
    public UUID id;
}
